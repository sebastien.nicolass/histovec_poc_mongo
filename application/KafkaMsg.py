import os
import time
from avro.io import AvroTypeException
from confluent_kafka.avro import AvroProducer, AvroConsumer, SerializerError
from confluent_kafka import avro
from typing import NoReturn
from Common import Common


class KafkaUtils:
    @staticmethod
    def load_avro_schema_from_files(avro_fields: tuple) -> tuple:
        """
        lit un schema AVRO
        """
        key_schema_string = """
        {"type": "string"}
        """
        avro_header = """
            {
            "name": "MyClass",
            "type": "record",
            "namespace": "fr.minint.histovec",
            "fields": [
        """
        avro_footer = """
          ]
        }
        """

        avro_parts = []
        for file in avro_fields:
            with open(os.getcwd() + '/avro/%s.part.avsc' % file, "r") as fh:
                avro_parts.append("".join(fh.readlines()))
        value_string = avro_header + ",".join(avro_parts) + avro_footer
        return avro.loads(key_schema_string), avro.loads(value_string)

    @staticmethod
    def load_avro_schema_from_file(avro_file: str) -> tuple:
        """
        lit un schema AVRO
        """
        key_schema_string = """
        {"type": "string"}
        """
        return avro.loads(key_schema_string), avro.load(avro_file)

    @staticmethod
    def get_producer(bootstrap_servers, schema_registry_url, key_schema, value_schema) -> AvroProducer:
        """
        renvoie le producer associé à un fichier de description de donnée
        """
        return AvroProducer(
            {
                "bootstrap.servers": bootstrap_servers,
                "schema.registry.url": schema_registry_url
            },
            default_key_schema=key_schema,
            default_value_schema=value_schema)

    @staticmethod

    def get_consumer(bootstrap_servers: str, schema_registry_url: str, topic: str, group_id: str) -> AvroConsumer:
        consumer = AvroConsumer({
            "bootstrap.servers": bootstrap_servers,
            "schema.registry.url": schema_registry_url,
            'group.id': group_id,
            'auto.offset.reset': 'earliest'
        })
        consumer.subscribe([topic])
        return consumer


class KafkaOutputData:
    def __init__(self):
        common = Common()
        avro_file = common.config['KAFKA']['data_avro_file']
        topic = common.config['KAFKA']['data_topic']
        self.logs = KafkaLogs()
        self.avro_file = os.getcwd() + "/avro/" + avro_file
        self.avro_fields = (
            "chunck", "date_update", "plaq_immat", "date_emission_CI", "marque", "tvv", "nom_commercial", "num_cnit",
            "couleur", "type_reception", "pt_tech_adm_f1", "ptac_f2", "ptav_g1", "pt_service_g", "ptra_f3", "ctec",
            "pers", "divers", "sit_adm", "historique", "pve")
        self.avro_file = os.getcwd() + "/avro/" + avro_file
        self.topic = topic
        self.bootstrap_servers = common.config['KAFKA']['bootstrap_servers']
        self.schema_registry_url = common.config['KAFKA']['schema_registry_url']

        self.key_schema, self.value_schema = KafkaUtils.load_avro_schema_from_files(self.avro_fields)
        self.producer = KafkaUtils.get_producer(self.bootstrap_servers, self.schema_registry_url, self.key_schema,
                                                self.value_schema)

        self.logger = KafkaLogs()

    def get_consumer(self, group_id: str) -> AvroConsumer:
        """

        :param group_id:
        :return:
        """
        return KafkaUtils.get_consumer(self.bootstrap_servers, self.schema_registry_url, self.topic, group_id)

    def send_record(self, msg_key: str, msg_value: dict) -> NoReturn:
        """
        Envoi un message sur le bus Kafka
        :param msg_key:
        :param msg_value:
        :return:
        """
        try:
            self.producer.produce(topic=self.topic, key=msg_key, value=msg_value)
        except SerializerError:
            message = {
                'date_event': time.time(),
                'level': 'ERROR',
                'entity': 'READ_CHUNCK',
                'verb': "INJECTION MODEL ERRROR",
                'indicator': 0,
                'text': msg_key + ' ' + msg_value['plaq_immat']
            }

            self.logger.send_logs(msg_key, message)
            pass
        except AvroTypeException:
            message = {
                'date_event': time.time(),
                'level': 'ERROR',
                'entity': 'READ_CHUNCK',
                'verb': "INJECTION MODEL TYPE ERRROR",
                'indicator': 0,
                'text': msg_key + ' ' + msg_value['plaq_immat']
            }
            self.logger.send_logs(msg_key, message)
            pass


class KafkaLogs:
    def __init__(self):
        common = Common()
        avro_file = os.getcwd() + "/avro/" + common.config['KAFKA']['logs_avro_file']
        key_schema, value_schema = KafkaUtils.load_avro_schema_from_file(avro_file)
        self.bootstrap_servers = common.config['KAFKA']['bootstrap_servers']
        self.schema_registry_url = common.config['KAFKA']['schema_registry_url']
        self.topic = common.config['KAFKA']['logs_topic']

        self.producer = KafkaUtils.get_producer(self.bootstrap_servers, self.schema_registry_url, key_schema,
                                                value_schema)

    def send_logs(self, msg_key: str, msg_value: dict) -> NoReturn:
        """

        :param msg_key:
        :param msg_value:
        :return:
        """
        self.send_logs_to_kafka(msg_key, msg_value)

    def send_logs_to_kafka(self, msg_key: str, msg_value: dict) -> NoReturn:
        """
        Envoi un message sur le bus Kafka
        :param msg_key:
        :param msg_value:
        :return:
        """
        try:
            self.producer.produce(topic=self.topic, key=msg_key, value=msg_value)
            self.producer.flush()
        except Exception as e:
            print("Cannot send logs")
            exit()
