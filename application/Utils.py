from typing import NoReturn
import time

import pymongo.collection
from pymongo import MongoClient
import os
import glob
import csv
import json
import datetime

from Common import Common
from KafkaMsg import KafkaLogs, KafkaOutputData


class Utils:

    @staticmethod
    def check_fields(in_avro, in_data, kafka_logs) -> NoReturn:
        """
        vérifie que tous les champs présents dans le fichier de chunk sont décrit dans le fichier avro
        :param in_avro:
        :param in_data:
        :param kafka_logs:
        :return: rien
        """
        for data_key in in_data:
            if data_key != 'utac_id' and data_key not in in_avro:
                Utils.send_logs(kafka_logs, msg_key=None, level='ERROR', entity='READ_CHUNCK', verb='AVRO SCHEME',
                                indicator=0, text="missing data key %s in avro" % data_key)

    @staticmethod
    def send_logs(kafka, msg_key, level='INFO', entity='UNKNOWN', verb='', indicator: float = 0.0, text='') -> NoReturn:
        """
        Send logs to the dedicated kafka queue
        :param kafka:
        :param msg_key:
        :param level:
        :param entity:
        :param verb:
        :param indicator:
        :param text:
        :return:
        """
        if msg_key is None:
            msg_key = str(time.time())
        message = {
            'date_event': time.time(),
            'level': level,
            'entity': entity,
            'verb': verb,
            'indicator': indicator,
            'text': text
        }
        kafka.send_logs_to_kafka(msg_key, message)
        kafka.producer.flush()

    @staticmethod
    def get_mongo(database: str, collection: str) -> pymongo.collection.Collection:
        """
        Get mongo connection to a specific database/collection
        :param database:
        :param collection:
        :return:
        """
        mongo_host, mongo_port = Common.get_mongo()
        mongo_client = MongoClient(
            mongo_host,
            mongo_port
        )
        mongo_db = mongo_client[database]
        return mongo_db[collection]

    @staticmethod
    def process_line(chunck: str, data_key: str, data: dict, to_kafka: dict, kafka_logs: KafkaLogs,
                     kafka_data: KafkaOutputData, do_check_fields: bool = False) -> NoReturn:
        """
                Process one line of data

        :param chunck:
        :param data_key:
        :param data:
        :param to_kafka:
        :param kafka_logs:
        :param kafka_data:
        :param do_check_fields:
        :return:
        """
        data['chunck'] = chunck
        send_to_kafka = {}
        for key in to_kafka:
            send_to_kafka[key] = data[to_kafka[key]['field']] \
                if to_kafka[key]['field'] in data else to_kafka[key]['default']

        # postprocess
        for key in send_to_kafka:
            if to_kafka[key]['function'] is not None:
                send_to_kafka[key] = getattr(DataProcess, to_kafka[key]['function'])(send_to_kafka, key)

        if do_check_fields:
            Utils.check_fields(kafka_data.value_schema.fields_dict.keys(), data.keys(), kafka_logs)

        if True:
            send_to_kafka['_id'] = data_key
            print(data_key)
            try:
                kafka_data.send_record(data_key, send_to_kafka)
            except Exception as e:
                print(e)
        kafka_data.producer.flush()
        kafka_logs.producer.flush()

    @staticmethod
    def read_one_chunck(chunck: str, to_kafka: dict, kafka_logs: KafkaLogs, kafka_data: KafkaOutputData) -> NoReturn:
        """

        :param chunck:
        :param to_kafka:
        :param kafka_logs:
        :param kafka_data:
        :return:
        """
        with open(chunck) as fh:
            nb_injected = 0
            start = time.time()
            Utils.send_logs(kafka_logs,
                            msg_key=None,
                            verb="START INJECTION",
                            entity='READ_CHUNCK',
                            text="Start processing %s" % chunck)
            csv_reader = csv.reader(fh, delimiter=',', quotechar='"')
            chunck_name = chunck.replace(os.getcwd() + '/../input_data/', '')
            for line in csv_reader:
                Utils.process_line(chunck_name, line[0], json.loads(line[1]), to_kafka, kafka_logs, kafka_data,
                                   do_check_fields=True)
                nb_injected += 1
        Utils.send_logs(kafka_logs,
                        msg_key=None,
                        entity='READ_CHUNCK',
                        verb="NUMBER_VEHICLES",
                        text="End processing %s" % chunck,
                        indicator=nb_injected)
        Utils.send_logs(kafka_logs,
                        msg_key=None,
                        entity='READ_CHUNCK',
                        verb="DURATION",
                        text="Processing duration for %s " % chunck,
                        indicator=time.time() - start)

    @staticmethod
    def get_data_from_filesystem(config, to_kafka, kafka_data):
        if config['INPUT_DATA']['path'] == 'absolute':
            path = config['INPUT_DATA']['folder']
        else:
            path = os.getcwd() + config['INPUT_DATA']['folder']
        files = sorted([f for f in glob.glob(path + "*_input.csv", recursive=False)])
        kafka_logs = KafkaLogs()
        for chunck in files:
            Utils.read_one_chunck(chunck, to_kafka, kafka_logs, kafka_data)
            kafka_data.producer.flush()
            kafka_logs.producer.flush()


class DataProcess:

    @staticmethod
    def oui_non_to_boolean(input_data: dict, key: str) -> bool:
        if input_data[key] == 'OUI':
            return True
        return False

    @staticmethod
    def dict_to_json(input_data: dict, key: str) -> str:
        return json.dumps(input_data[key])

    @staticmethod
    def str_to_timestamp(input_data: dict, key: str) -> int:
        if type(input_data[key]) is int:
            return 0
        return int(time.mktime(datetime.datetime.strptime(input_data[key], '%d/%m/%Y').timetuple()))
