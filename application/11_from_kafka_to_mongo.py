import json
from Utils import Utils
from KafkaMsg import KafkaOutputData
from Common import Common

common = Common()
kafka_data = KafkaOutputData()
consumer = kafka_data.get_consumer(common.config['KAFKA']['mongo_consumer_group_id'])
collection = Utils.get_mongo('local', 'histovec_kafka_chunck')

while True:
    message = consumer.poll(10)
    if message is not None:
        vehicule = message.value()
        vehicule_id = vehicule['_id']
        del vehicule['_id']
        collection.update_one({'_id': vehicule_id}, {'$set': vehicule}, upsert=True)



