from Utils import Utils
from Common import Common

from KafkaMsg import KafkaOutputData


common = Common()
kafka_data = KafkaOutputData()

to_kafka = {}
for field in kafka_data.value_schema.fields:
    if 'field' in field.other_props:
        to_kafka[field.name] = {'default': field.default, 'field': field.other_props['field'], 'function': None}
    else:
        to_kafka[field.name] = {'default': field.default, 'field': field.name, 'function': None}
    if 'function' in field.other_props:
        to_kafka[field.name]['function'] = field.other_props['function']
if common.config['INPUT_DATA']['source'] == 'filesystem':
    Utils.get_data_from_filesystem(common.config, to_kafka, kafka_data)
elif common.config['INPUT_DATA']['source'] == 'swift':
    pass



